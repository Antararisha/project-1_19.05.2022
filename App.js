
const weatherApi = {
    key: "4c8e978b830723629bff08dd906f8a26",
    baseUrl : "https://api.openweathermap.org/data/2.5/weather"

}



//geoLocation part
const myLocation =()=>{
    const location =document.querySelector('.button1');

    const success=(position) =>{
        console.log(position);
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        // console.log(latitude , longitude);
        getCurrentLocationWeatherReport(latitude,longitude);
    }
    const error =()=>{
        location.textContent='Unable to fetch your Location';
    }

    navigator.geolocation.getCurrentPosition(success,error);

}
document.querySelector('.button1').addEventListener('click',myLocation);

function getCurrentLocationWeatherReport(latitude,longitude) {
    // fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${weatherApi.key}&units=metric`)
    fetch(`https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${latitude}&longitude=${longitude}&localityLanguage=en`)
    .then(res => res.json()).then(data =>{
       currentCity= Location.textContent = data.countryName;
       console.log(currentCity);
       getWeatherReport(currentCity);
       showWeatherReport(currentCity);
    // document.querySelector('.weather-body').style.display = "block";
    // document.querySelector('#iconsContainer').style.display = "block";
    })
}


//input Location
const searchInputBox =document.getElementById('input-box');

searchInputBox.addEventListener('keypress',(event) => {
    if(event.keyCode == 13){
    console.log(searchInputBox.value);
    getWeatherReport(searchInputBox.value);
    getForecastrReport(searchInputBox.value);
    document.querySelector('.weather-body').style.display = "block";
    document.querySelector('#iconsContainer').style.display = "block";
    }
});

const forecastApi= {
    key: "4c8e978b830723629bff08dd906f8a26",
    baseUrl2: "https://api.openweathermap.org/data/2.5/forecast"
}

function getWeatherReport(city) {
    fetch(`${weatherApi.baseUrl}?q=${city}&appid=${weatherApi.key}&units=metric`)
    .then(weather => {
        return weather.json();
    }).then(showWeatherReport);
}
function getForecastrReport(city) {
    fetch(`${forecastApi.baseUrl2}?q=${city}&appid=${forecastApi.key}&units=metric`)
    .then(forecast => {
        return forecast.json();
    }).then(showForecastReport);
}

function showForecastReport(forecast){
console.log(forecast);
for(i = 0; i<5; i++){
    document.getElementById("day" + (i+1) + "Min").innerHTML = "Min: " + `${Math.round(forecast.list[i].main.temp_min)}&deg;C`;
}
    
for(i = 0; i<5; i++){
    document.getElementById("day" + (i+1) + "Max").innerHTML = "Max: " + `${Math.round(forecast.list[i].main.temp_max)}&deg;C`;
}

for(i = 0; i<5; i++){
    let forecastIcon = document.getElementById("img" + (i+1));
    let forecasticonID=`${forecast.list[i].weather[0].icon}`;
    let forecasticonURL = "http://openweathermap.org/img/wn/"+forecasticonID+"@2x.png";
    forecastIcon.innerHTML = `<img src=${forecasticonURL}>`;
    

}

}
function showWeatherReport(weather){
    console.log(weather);

    let city = document.getElementById('city');
    city.innerHTML = `${weather.name}, ${weather.sys.country}`;
    
    let temperature = document.getElementById('temp');
    temperature.innerHTML = `${Math.round(weather.main.temp)}&deg;C`;

    let temperatureF = document.getElementById('temp2');
    temperatureF.innerHTML = `${(Math.round(weather.main.temp)* 9) / 5 + 32}&deg;F`

    // function Fahrenheit(temperature2){
    //     let temperature3 = document.getElementById('temp');
    //     var Fahrenheit = (temperature3* 9) / 5 + 32;
    //     console.log(Fahrenheit);
    //     // document.getElementById('temp').innerText = `${Fahrenheit}F`;
    // }
    
    let minmaxTemp = document.getElementById('min-max');
    minmaxTemp.innerHTML = `${Math.floor(weather.main.temp_min)}&deg;C (min)/${Math.ceil(weather.main.temp_max)}&deg;C (max)`;

    let weatherType = document.getElementById('weather');
    weatherType.innerHTML = `${weather.weather[0].main}`;

    let weatherIcon = document.getElementById('weather-icon');
    let iconId =`${weather.weather[0].icon}`;
    let iconURL = "http://openweathermap.org/img/wn/" + iconId + "@2x.png";
    weatherIcon.innerHTML = `<img src=${iconURL}>`;

    if(weatherType.textContent == 'Clear'){
        document.body.style.backgroundImage = "url('/images/clear.jpg')";
    }
    else if(weatherType.textContent == 'Clouds'){
        document.body.style.backgroundImage = "url('/images/cloudy.jpg')";
    }
    else if(weatherType.textContent == 'Haze'){
        document.body.style.backgroundImage = "url('/images/Haze.jpg')";
    }
    else if(weatherType.textContent == 'Rain'){
        document.body.style.backgroundImage = "url('/images/Rain.jpg')";
    }
    else if(weatherType.textContent == 'Snow'){
        document.body.style.backgroundImage = "url('/images/Snow.jpg')";
    }
    else if(weatherType.textContent == 'Thunderstrom'){
        document.body.style.backgroundImage = "url('/images/Thunderstrom.jpg')";
    }
    // function setCityWeatherIcon(iconID) {
    //     let iconURL = "http://openweathermap.org/img/wn/" + iconID + "@2x.png";
    //     document.getElementById(
    //       "city-weather-icon"
    //     ).innerHTML = `<img src=${iconURL} width='400px' height='150px'>`;
    //   }
    //   setCityWeatherIcon(data.weather[0].icon);
    

    let date = document.getElementById('date');
    var today = new Date();
    // var dd = today.getDate();
    // var mm = today.getMonth(); 
    // var yyyy = today.getFullYear();
    // date.innerHTML = `${dd},${mm},${yyyy}`;
    var options = { weekday: 'long', day: 'numeric', year: 'numeric', month: 'long' };
    date.innerHTML = today.toLocaleDateString("en-US", options);

}

